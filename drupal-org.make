api = 2
core = 7.x

; Contrib modules
projects[backup_migrate] = 3.1
projects[ctools] = 1.9
projects[date] = 2.9
projects[entity] = 1.6
projects[features] = 1.0
projects[feeds] = 2.0-beta1
projects[feeds_ofx] = 1.2
projects[feeds_review] = 1.x
projects[feeds_tamper] = 1.1
projects[feeds_xpathparser] = 1.1
projects[fraction] = 1.3
projects[job_scheduler] = 2.0-alpha3
projects[strongarm] = 2.0
projects[views] = 3.13
projects[views_bulk_operations] = 3.3
projects[views_tree] = 2.0

; Ledger modules
projects[ledger] = 1.x
projects[ledger_import] = 1.x

