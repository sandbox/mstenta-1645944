api = 2
core = 7.x

; -----------------------------------------------------------------------------
; Drupal core
; -----------------------------------------------------------------------------

projects[drupal][type] = core
projects[drupal][version] = 7.41

; -----------------------------------------------------------------------------
; Drupal Ledger installation profile
; -----------------------------------------------------------------------------

projects[drupal_ledger][type] = profile
projects[drupal_ledger][download][type] = git
projects[drupal_ledger][download][url] = http://git.drupal.org/sandbox/mstenta/1645944.git
projects[drupal_ledger][download][branch] = 7.x-1.x

